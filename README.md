# ativ-nuxt-crud-api-fake

> Nuxt.js TypeScript project

## Project Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# Validate or fix code with the rules of the extends: plugin:vue/recommended, @vue/airbnb
$ npm run lint

# build for production and launch server
$ npm run build
$ npm start

# generate static project (root /dist)
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
