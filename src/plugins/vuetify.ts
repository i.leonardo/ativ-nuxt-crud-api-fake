import { Vue } from 'nuxt-property-decorator';
import Vuetify from 'vuetify/lib';

const { pt } = require('vuetify/lib/locale');

Vue.use(Vuetify, {
  iconfont: 'md',
  lang: {
    locales: {
      pt,
    },
    current: 'pt',
  },
});
