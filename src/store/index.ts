import { set, toggle } from '@/utils/vuex';

export const state = () => ({
  msg: {
    bool: false,
    color: '',
    message: '',
  },
  modal: false,
});

export const mutations = {
  setMsg: set('msg'),
  setBoolMsg: set('msg.bool'),
  setModal: set('modal'),
  toggleModal: toggle('modal'),
};
