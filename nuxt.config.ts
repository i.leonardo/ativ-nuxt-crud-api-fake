import NuxtConfiguration from '@nuxt/config';
import pkg from './package.json';

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

const config: NuxtConfiguration = {
  mode: 'universal',

  srcDir: 'src/',

  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' },
    ],
  },

  loading: { color: '#3b8070' },

  router: {
    base: process.env.NODE_ENV === 'production'
      ? '/ativ-nuxt-crud-api-fake/'
      : '/',
  },

  css: [
    'vuetify/src/stylus/app.styl',
  ],

  plugins: [
    '@/plugins/vuetify.ts',
  ],

  modules: [
    '@nuxtjs/axios',
  ],

  axios: {
    baseURL: 'https://my-json-server.typicode.com/pedroskakum/fake-api',
  },

  build: {
    transpile: ['vuetify/lib'],

    plugins: [
      new VuetifyLoaderPlugin(),
    ],

    postcss: {
      plugins: {
        autoprefixer: {},
      },
    },

    extend(_, { isClient, loaders: { vue } }) {
      if (isClient) {
        // eslint-disable-next-line no-param-reassign
        vue.transformAssetUrls = {};
      }
    },
  },
};

export default config;
